﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

    public int hitPoints = 2;


	void Start () {
	
	}
	
    public void DamageWall(int damageReceiced)
    {
        hitPoints -= damageReceiced;
        if(hitPoints <= 0)
        {
            gameObject.SetActive(false);
        }
    }

}
